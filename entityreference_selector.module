<?php

/**
 * Implements hook_field_widget_info().
 */
function entityreference_selector_field_widget_info() {
  $widgets['entityreference_selector'] = array(
    'label' => t('Selector'),
    'description' => t('An advanced, VBO widget.'),
    'field types' => array('entityreference'),
    'behaviors' => array(
      'multiple values' => FIELD_BEHAVIOR_CUSTOM,
      'default value' => FIELD_BEHAVIOR_NONE,
    ),
  );

  return $widgets;
}

/**
 * Implements hook_field_widget_settings_form().
 */
function entityreference_selector_field_widget_settings_form($field, $instance) {
  $widget = $instance['widget'];
  $settings = $widget['settings'];

  if ($widget['type'] == 'entityreference_selector') {
    // Get a list of all views that contain a "Entityreference View Widget" display.
    $options = array();
    $displays = views_get_applicable_views('entityreference selector');
    foreach ($displays as $data) {
      list($view, $display_id) = $data;
      $view_name = !empty($view->human_name) ? $view->human_name : $view->name;
      $options[$view->name . '|' . $display_id] = check_plain($view_name . ' | ' . $view->display_handler->display->display_title);
    }

    $element['selected_view'] = array(
      '#type' => 'select',
      '#title' => t('Selected view'),
      '#description' => t('Specify the View to use for displaying selected items on a form. Only views that have an "Entityreference View Widget" display are shown.'),
      '#options' => $options,
      '#default_value' => isset($settings['selected_view']) ? $settings['selected_view'] : '',
      '#required' => TRUE,
    );
    $element['modal_view'] = array(
      '#type' => 'select',
      '#title' => t('Modal view'),
      '#description' => t('Specify the View to use for selecting items in the modal window. Only views that have an "Entityreference View Widget" display are shown.'),
      '#options' => $options,
      '#default_value' => isset($settings['modal_view']) ? $settings['modal_view'] : '',
      '#required' => TRUE,
    );
  }

  return $element;
}

/**
 * Implements hook_field_widget_form().
 */
function entityreference_selector_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  drupal_flush_all_caches();

  ctools_include('ajax');
  ctools_include('modal');
  ctools_include('plugins');

  $ids = array();
  foreach ($items as $delta => $item) {
    $ids[] = $item['target_id'];
  }

  $selected_view = explode('|', $instance['widget']['settings']['selected_view']);
  $modal_view = explode('|', $instance['widget']['settings']['modal_view']);

  $field_name = $field['field_name'];
  $entity_type = $instance['entity_type'];
  $bundle = $instance['bundle'];

  $field_name_id_array = array(
    'ers', $entity_type, $bundle, $field_name
  );

  $field_name_id = implode('-', $field_name_id_array);

  $selected_params = array(
    'field_name_id' => $field_name_id,
    'selected_view' => $selected_view,
    'modal_view' => $modal_view,
    'ids' => $ids,
  );
  $html = entityreference_selector_selected_list($selected_params);
  $html = '<div id="' . $field_name_id . '" class="ers-selected-wrapper">' . $html . '</div>';

  $element['ers_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t($instance['label']),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $element['ers_wrapper']['list'] = array(
    '#markup' => $html,
  );

  $settings = array(
    'langcode' => $langcode,
    'target_type' => $field['settings']['target_type'],
    'target_bundles' => $field['settings']['handler_settings']['target_bundles'],
    'cardinality' => $field['cardinality'],
    'required' => $instance['required'],
    'field_label' => $instance['label'],
    'selected_view' => implode('|', $selected_view),
    'modal_view' => implode('|', $modal_view),
    'field_name_id' => $field_name_id,
    'ids_json' => json_encode($ids),
  );

  $selected_ids = $selected_params['ids'];
  $element['ers_wrapper']['target_id'] = array(
    '#type' => 'hidden',
    '#value' => implode($selected_ids, '+'),
    '#attributes' => array(
      'id' => $field_name_id . '-target-id',
    ),
    '#element_validate' => array('entityreference_selector_field_widget_validate'),
    '#settings' => array(
      'required' => $instance['required'],
    ),
  );

  drupal_add_js(array('entityreferenceSelector' => array($field_name_id => $settings)), 'setting');

  return $element;
}


/**
 * Pushes input values to form state.
 */
function entityreference_selector_field_widget_validate($element, &$form_state, $form) {
  array_pop($element['#parents']);
  $input = drupal_array_get_nested_value($form_state['input'], $element['#parents']);
  $ids = explode('+', $input['target_id']);
  if (!empty($ids)) {
    $entries = array();
    foreach ($ids as $delta => $value) {
      if (!empty($value)) {
        $entries[] = array('target_id' => $value);
        $valid = TRUE;
      }
    }
    array_pop($element['#parents']);
    form_set_value($element, $entries, $form_state);
  }
  if ($element['#settings']['required'] && empty($valid)) {
    form_set_error($element['#settings']['element'], t('@f field is required!', array('@f' => $element['#settings']['field_label'])));
  }
}

/**
 * Implements hook_menu().
 */
function entityreference_selector_menu() {
  $items['entityreference_selector/%ctools_js/modal/%/%/%'] = array(
    'title' => 'Entity reference selector modal',
    'page callback' => 'entityreference_selector_modal_callback',
    'page arguments' => array(1, 3, 4, 5),
    'access callback' => TRUE,
    'theme callback' => 'ajax_base_page_theme',
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Callback for ctools modal window.
 *
 * @param string $mode
 *   Mode: js or nojs.
 * @param string $modal_view_name
 *   Machine name of the modal view.
 * @param string $modal_view_display
 *   Display name of the modal view.
 * @param string $field_name_id
 *   The name of the Entity reference field.
 */
function entityreference_selector_modal_callback($mode, $modal_view_name, $modal_view_display, $field_name_id) {
  ctools_include('modal');
  ctools_modal_add_js();

  drupal_add_js(drupal_get_path('module', 'entityreference_selector') . '/entityreference_selector.js');
  drupal_add_css(drupal_get_path('module', 'entityreference_selector') . '/entityreference_selector.css');

  $ids = array();
  if(arg(6)) {
    $ids = explode('+', arg(6));
  }

  $html = entityreference_selector_modal($modal_view_name, $modal_view_display, $field_name_id, $ids);

  return ctools_modal_render(t('Selector'), $html);
}

/**
 * Content of ERS modal.
 *
 * @param string $modal_view_name
 *   Machine name of the modal view.
 * @param string $modal_view_display
 *   Display name of the modal view.
 * @param string $field_name_id
 *   Html ID of the Entity reference field. For reverse it must have "ers_reverse-" prefi, otherwise "ers-".
 * @param array $ids
 *   Array of entity ids already selected.
 */

function entityreference_selector_modal($modal_view_name, $modal_view_display, $field_name_id, $ids = NULL) {
  $args = array(
    'field_name_id' => $field_name_id,
  );

  $view = views_get_view($modal_view_name);
  $view->set_display($modal_view_display);
  $view->use_ajax = TRUE;
  $view->display[$modal_view_display]->display_options['use_ajax'] = TRUE;

  $view->display['default']->display_options['use_ajax'] = TRUE;
  $view->display_handler->options['use_ajax'] = TRUE;
  $view->display_handler->default_display->options['use_ajax'] = TRUE;

  if(!empty($ids)) {
    $ids_string = implode('+', $ids);
    $selected_view = views_get_view($modal_view_name);
    $selected_view->set_display($modal_view_display);
    $selected_view->set_items_per_page(0);
    $selected_view->use_pager(FALSE);
    $selected_view->set_arguments(array($ids_string));
  }

  $html = theme('status_messages');

  if (method_exists($view, 'preview')) {
    $html .= $view->preview();

    if(isset($selected_view) && method_exists($selected_view, 'preview')) {
      $html .= '<div class="ers-selected-entities element-hidden">' . $selected_view->preview() . '</div>';
    }

    $modal_form = drupal_get_form('entityreference_selector_modal_form', $args);

    $html =
      '<div id="ers-modal">
        <div id="ers-modal-available" data-ers="' . $field_name_id . '">' . $html . '</div>
        <div id="ers-modal-toolbox">' . drupal_render($modal_form) . '</div>
      </div>';
  }
  else {
    $html = '<div class="messages error">' . t("The target view can't be loaded.") . '</div>';
  }

  return $html;
}

/**
 * Form in ERS modal, that submits selections.
 *
 * @param $form
 * @param $form_state
 * @param $args
 * @return mixed
 */

function entityreference_selector_modal_form($form, &$form_state, $args) {

  $field_properties = explode('-', $args['field_name_id']);

  if($field_properties[0] == 'ers_reverse') {
    $field_info = field_info_field($field_properties[3]);
    $target_type = $field_properties[1];
    $target_bundles = $field_info['bundles'][$target_type];
  } else {
    $field_info = field_info_field($field_properties[3]);
    $target_type = $field_info['settings']['target_type'];
    $target_bundles = $field_info['settings']['handler_settings']['target_bundles'];
  }

  $class = array();

  if($target_type == 'node') {
    $form['add_new'] = array(
      '#type' => 'container',
      '#weight' => 0,
      '#prefix' => '<div class="ers-toolbox-element">',
      '#suffix' => '</div>',
    );
    foreach($target_bundles as $target_bundle) {
      $label = ers_entity_bundle_label($target_type, $target_bundle);

      $form_element_id = 'add_' . $target_bundle;
      $form['add_new'][$form_element_id] = array(
        '#type' => 'button',
        '#name' => $target_bundle,
        '#ajax' => array(
          'callback' => 'entityreference_selector_modal_add_new_callback',
          'wrapper' => 'ers-modal',
          'method' => 'replace',
        ),
        '#value' => t('Create @type', array('@type' => $label)),
      );
    }
    $class[] = "ers-add-new-avaliable";
  }


  $form['selected_ids'] = array(
    '#markup' => '<div id="ers-modal-selected-wrapper"><div id="ers-modal-selected"></div></div>',
    '#prefix' => '<div class="ers-toolbox-element">',
    '#suffix' => '</div>',
  );

  $form['selected_view'] = array(
    '#type' => 'hidden',
  );

  $form['modal_view'] = array(
    '#type' => 'hidden',
  );

  $form['field_name_id'] = array(
    '#type' => 'hidden',
    '#value' => $args['field_name_id'],
  );

  $form['ids_json'] = array(
    '#type' => 'hidden',
  );

  $form['actions'] = array(
    '#type' => 'actions',
    '#prefix' => '<div class="ers-toolbox-element">',
    '#suffix' => '</div>',
  );
  $form['actions']['submit_close'] = array(
    '#type' => 'submit',
    '#ajax' => array(
      'callback' => 'entityreference_selector_modal_form_callback',
    ),
    '#value' => t('Complete selection and close'),
  );

  $form['#attributes'] = array(
    'class' => $class,
  );

  return $form;
}


function entityreference_selector_modal_add_new_callback($form, $form_state) {

  global $user;
  // If people aren't using javascript, then I just boot em. sorry. its 2011.
  //if (!$js) return "Javascript required";

  // Include your ctools crap here
  ctools_include('node.pages', 'node', '');
  ctools_include('modal');
  ctools_include('ajax');

  $node_type = $form_state['triggering_element']['#name'];

  // Create a blank node object here. You can also set values for your custom fields here as well.
  $node = (object) array(
    'uid' => $user->uid,
    'name' => (isset($user->name) ? $user->name : ''),
    'type' => $node_type,
    'language' => LANGUAGE_NONE,
  );

  $form_state_new = array(
    'ers' => array(
      'modal_view' => $form_state['values']['modal_view'],
      'ids' => json_decode($form_state['values']['ids_json']),
      'field_name_id' => $form_state['values']['field_name_id'],
    )
  );

  $form_state_new['build_info']['args'] = array($node);

  $form_id_new = $node_type . '_node_form';

  $node_add_form = drupal_build_form($form_id_new, $form_state_new);

  $output = '<div id="ers-modal-node-add">' . theme('status_messages') . drupal_render($node_add_form) . '</div>';

  return $output;
}

/**
 * Implements hook_form_alter()
 */
function entityreference_selector_form_alter(&$form, &$form_state, $form_id) {
  // Ajaxify forms that are opened in ERS modal
  if (isset($form_state['ers'])) {
    form_load_include($form_state, 'inc', 'node', 'node.pages');
    $form['actions']['submit']['#ajax'] = array(
      'callback' => 'entityreference_selector_node_add_ajax_callback',
    );
    if(isset($form['actions']['preview'])) {
      $form['actions']['preview']['#access'] = FALSE;
    }
    $form['actions']['cancel'] = array(
      '#type' => 'button',
      '#value' => t('Cancel'),
      '#ajax' => array(
        'callback' => 'entityreference_selector_node_add_ajax_callback',
      ),
      '#limit_validation_errors' => array(),
      '#name' => 'cancel',
      '#weight' => 50,
    );
  }
}

/**
 * AJAX call back on node add form.
 *
 * @param $form
 * @param $form_state
 * @return array
 */
function entityreference_selector_node_add_ajax_callback($form, &$form_state) {
  $commands = array();

  $ids = $form_state['ers']['ids'];
  $modal_view = explode('|', $form_state['ers']['modal_view']);
  $modal_view_name = $modal_view[0];
  $modal_view_display = $modal_view[1];
  $field_name_id = $form_state['ers']['field_name_id'];

  if($form_state['triggering_element']['#name'] == 'cancel') {
    $html = entityreference_selector_modal($modal_view_name, $modal_view_display, $field_name_id, $ids);
    $commands[] = ajax_command_html('#modal-content', $html);
  }
  elseif (isset($form_state['node']->nid)) {
    $nid = $form_state['node']->nid;
    $ids[] = $nid;
    $html = entityreference_selector_modal($modal_view_name, $modal_view_display, $field_name_id, $ids);
    $commands[] = ajax_command_invoke(NULL, "ercAddId", array($field_name_id, $nid));
    $commands[] = ajax_command_html('#modal-content', $html);
  } else {
    $output = '<div id="ers-modal-node-add">' . theme('status_messages') . drupal_render($form) . '</div>';
    $commands[] = ajax_command_html('#modal-content', $output);
  }

  return array('#type' => 'ajax', '#commands' => $commands);
}

function ers_entity_bundle_label($input_entity_type, $input_bundle) {
  $labels = &drupal_static(__FUNCTION__, array());

  if (empty($labels)) {
    foreach (entity_get_info() as $entity_type => $entity_info) {
      foreach ($entity_info['bundles'] as $bundle => $bundle_info) {
        $labels[$entity_type][$bundle] = !empty($bundle_info['label']) ? $bundle_info['label'] : $bundle;
       }
    }
  }

  return isset($labels[$input_entity_type][$input_bundle]) ? $labels[$input_entity_type][$input_bundle] : $input_bundle;
}


function entityreference_selector_modal_form_callback($form, $form_state) {

  $values = $form_state['values'];

  $selected_view = $values['selected_view'];
  $modal_view = $values['modal_view'];
  $ids_json = $values['ids_json'];
  $field_name_id = $values['field_name_id'];

  $selected_params = array(
    'field_name_id' => $field_name_id,
    'selected_view' => explode('|', $selected_view),
    'modal_view' => explode('|', $modal_view),
    'ids' => json_decode($ids_json),
  );
  $output = entityreference_selector_selected_list($selected_params);

  $ers_div_selector = '#' . $field_name_id . ' .ers-selected';

  $commands = array();
  $commands[] = ajax_command_replace($ers_div_selector, $output);
  $commands[] = ctools_modal_command_dismiss();
  $commands[] = ajax_command_invoke(NULL, "ercPopulateField", array($field_name_id));

  drupal_alter('ers_commands', $commands, $field_name_id);

  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 *
 *
 * @param array $params
 * - field_name_id
 * - selected_view
 * - modal_view
 * - ids
 * @return string3
 */

function entityreference_selector_selected_list(&$params = array()) {
  ctools_include('ajax');
  ctools_include('modal');
  ctools_include('plugins');
  ctools_modal_add_js();

  $field_name_id = $params['field_name_id'];

  if(isset($params['initial_view'])) {
    // if inital view is set we use it to generate list and to get ids
    $selected_view = $params['initial_view'];
    $selected_view_arguments = $params['initial_view_arguments'];
    $selected_view_id_field = $params['initial_view_id_field'];
  } else {
    $selected_view = $params['selected_view'];
    $ids = $params['ids'];
    $selected_view_arguments = array(implode('+', $ids));
  }

  $modal_view = $params['modal_view'];

  $view = views_get_view($selected_view[0]);
  $view->set_display($selected_view[1]);
  $view->set_arguments($selected_view_arguments);
  $view->set_use_ajax(TRUE);
  $view->display['default']->display_options['use_ajax'] = TRUE;
  $view->display[$selected_view[1]]->display_options['use_ajax'] = TRUE;
  $view->display_handler->options['use_ajax'] = TRUE;
  $view->display_handler->default_display->options['use_ajax'] = TRUE;

  if (method_exists($view, 'preview')) {
    $html = $view->preview();
    if(isset($params['initial_view'])) {
      $ids = array();
      foreach ($view->result as $result) {
        $ids[] = $result->{$selected_view_id_field};
      }
    }
  }
  else {
    $html = '<div class="messages error">' . t("The target view can't be loaded.") . '</div>';
  }
  $params['ids'] = $ids;
  $ids_imploded = implode('+', $ids);
  $modal_url = 'entityreference_selector/nojs/modal/' . $modal_view[0] . '/' . $modal_view[1] . '/' . $field_name_id . '/' . $ids_imploded;
  $html .= ctools_modal_text_button(t('Browse'), $modal_url, '', 'button field-add-more-submit ers-add-items');

  return '<div class="ers-selected">' . $html . '</div>';
}

/**
 * Implements hook_views_api().
 */
function entityreference_selector_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'entityreference_selector') . '/views',
  );
}

/**
 * Implements hook_views_data_alter().
 */
function entityreference_selector_views_data_alter(&$data) {
  foreach (entity_get_info() as $info) {
    if (isset($info['base table']) && isset($data[$info['base table']]['table'])) {
      $data[$info['base table']]['ers_checkbox'] = array(
        'title' => $data[$info['base table']]['table']['group'],
        'group' => t('Entity Reference Selector'),
        'help' => t('Provide a checkbox to select the row for an entity reference.'),
        'real field' => $info['entity keys']['id'],
        'field' => array(
          'handler' => 'entityreference_selector_handler_ers_checkbox',
          'click sortable' => FALSE,
        ),
      );
    }
  }
}