<?php
/**
 * @file
 * entityreference_selector_example.features.inc
 */

/**
 * Implements hook_views_api().
 */
function entityreference_selector_example_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function entityreference_selector_example_node_info() {
  $items = array(
    'ers_example_child' => array(
      'name' => t('ERS Example Child'),
      'base' => 'node_content',
      'description' => t('Entity Reference Selector example Child content type.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'ers_example_parent' => array(
      'name' => t('ERS Example Parent'),
      'base' => 'node_content',
      'description' => t('Entity Reference Selector example Parent content type.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
