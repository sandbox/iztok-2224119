<?php
/**
 * @file
 * entityreference_selector_example.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function entityreference_selector_example_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-ers_example_child-field_ers_example_parent'
  $field_instances['node-ers_example_child-field_ers_example_parent'] = array(
    'bundle' => 'ers_example_child',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => 1,
        ),
        'type' => 'entityreference_label',
        'weight' => 0,
      ),
      'entityreference_view_widget' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ers_example_parent',
    'label' => 'Parent reference',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference_selector',
      'settings' => array(
        'modal_view' => 'ers_example|modal',
        'selected_view' => 'ers_example|widget',
      ),
      'type' => 'entityreference_selector',
      'weight' => -4,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Parent reference');

  return $field_instances;
}
