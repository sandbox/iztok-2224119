(function($) {
  Drupal.behaviors.entityreferenceSelector = {
    attach: function(context, settings) {
      var field_name_id = $('#ers-modal #ers-modal-available').data('ers');

      if(field_name_id == undefined) {
          return;
      }

      settings = Drupal.settings.entityreferenceSelector[field_name_id];

      checkboxes = '#ers-modal input[name="entity_ids[]"]';

      $("#ers-modal-toolbox").once("init", function(){
        settings.ids = new Array();
        var ids = JSON.parse(settings.ids_json);
        $(this).find('input[name="selected_view"]').val(settings['selected_view']);
        $(this).find('input[name="modal_view"]').val(settings['modal_view']);
        $.each(ids, function(index, value){
            $('input[name="entity_ids[]"][value='+value+']').attr('checked', 'checked').ersCheck(field_name_id);
        });
        $('#modalContent').addClass('ers-modal-type');

        if(settings.target_type == 'node') {
          var target_bundles = settings.target_bundles;
          $.each(target_bundles, function(index, value) {
            var content_type = {
              'machine_name' : value,
              'label' : value
            }
            var link = Drupal.theme('ers_modal_add_new', content_type, field_name_id);
            //$('#ers-modal-add-new').prepend(link);
          });
        }
      });
      $(checkboxes).die('change').live('change', function(){
          $(this).ersCheck(field_name_id);
      });

      var ids = settings.ids;
      $.each(ids, function(index, value){
          $('input[name="entity_ids[]"][value='+value+']').attr('checked', 'checked').ersCheck(field_name_id);
      });

    }
  }

  $.fn.ercPopulateField = function(field_name_id) {
    var settings = Drupal.settings.entityreferenceSelector[field_name_id];
    var ids = settings.ids;
    var ids_string = ids.join('+');
    $('#' + settings.field_name_id + '-target-id').val(ids_string);
  };

  $.fn.ercAddId = function(field_name_id, id) {
    Drupal.settings.entityreferenceSelector[field_name_id].ids.push(id);
    Drupal.settings.entityreferenceSelector[field_name_id].ids_json = JSON.stringify(Drupal.settings.entityreferenceSelector[field_name_id].ids);
  };

  $.fn.ersCheck = function(field_name_id) {
    settings = Drupal.settings.entityreferenceSelector[field_name_id];
    var id = $(this).val();
    if($(this).is(':checked')){
      if($.inArray(id, settings.ids) <= -1) {
        var html = Drupal.t("Unsuported views view format");
        if($(this).parents('.view-content').children('table').length > 0) {
          html = $(this).parents('td').html();
        } else {
          html = $(this).parents('.views-row').html();
        }
        rowContent = $('<div class="ers-modal-selected-row"/>').html(html);
        $("#ers-modal-selected").append(rowContent);
        $('#ers-modal input[name="entity_ids[]"][value="'+id+'"]').attr('checked', 'checked');
        settings.ids.push(id);
      }
    } else {
      $('#ers-modal input[name="entity_ids[]"][value="'+id+'"]').removeAttr('checked');
      $('#ers-modal-selected input[value="'+id+'"]').parents('div.ers-modal-selected-row').remove();
      settings.ids.splice($.inArray(id, settings.ids), 1);
    }
    var ids_json = JSON.stringify(settings.ids);
    Drupal.settings.entityreferenceSelector[field_name_id].ids_json = ids_json;
    $('#ers-modal input[name="ids_json"]').val(ids_json);
  }

  Drupal.theme.prototype.ers_modal_add_new = function(content_type, field_name_id) {
    var html_id = content_type.machine_name + '-add-new-ajax';
    var link_href = Drupal.settings.basePath + 'entityreference_selector/nojs/add/' + content_type.machine_name;
    var settings = Drupal.settings.entityreferenceSelector[field_name_id];
    var modalSettings = {
      'modal_view' : settings.modal_view,
      'ids' : settings.ids,
      'field_name_id' : field_name_id
    }
    link_href += '?' + $.param(modalSettings);
    var link = $('<a href="' + link_href + '">' + content_type.label + "</a>").addClass('ajax-processed button').attr('id', html_id);
    var element_settings = {
      'url' : link_href,
      'event' : 'click'
    }
    Drupal.ajax[html_id] = new Drupal.ajax(html_id, link, element_settings);
    return link;
  }

})(jQuery);


