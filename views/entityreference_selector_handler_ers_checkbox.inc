<?php

/**
 * @file
 * Definition of mymodule_handler_handlername.
 */

/**
 * Description of what my handler does.
 */
class entityreference_selector_handler_ers_checkbox extends views_handler_field {
  var $revision = FALSE;

  function render($values) {
    $entity_id = $values->{$this->view->base_field};
    if (empty($entity_id)) {
      return;
    }
    return '<input name="entity_ids[]" type="checkbox" value="' . $entity_id . '"/>';
  }
}