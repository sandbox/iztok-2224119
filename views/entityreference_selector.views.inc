<?php

/**
 * @file
 * Views definitions for ERS module.
 */

/**
 * Implements hook_views_plugins().
 */
function entityreference_selector_views_plugins() {
  $entity_tables = array();
  $tables = views_fetch_data();
  foreach ($tables as $table_name => $table) {
    if (!empty($table['table']['entity type'])) {
      $entity_tables[] = $table_name;
    }
  }

  $plugins = array(
    'display' => array(
      'entityreference_selector' => array(
        'title' => t('Entity Reference Selector'),
        'help' => t('Selects referenceable entities for an entity reference view widget.'),
        'handler' => 'entityreference_selector_plugin_display',
        'use ajax' => TRUE,
        'use pager' => TRUE,
        'use more' => FALSE,
        'accept attachments' => FALSE,
        'theme' => 'views_view',
        // Make this plugin only available to tables that map to an entity type and Search API indexes.
        'base' => $entity_tables,
        // Custom property, used with views_get_applicable_views() to retrieve
        // all views with a 'Entity Reference Selector' display.
        'entityreference selector' => TRUE,
      ),
    ),
  );
  return $plugins;
}